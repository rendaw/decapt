# Simple declarative package management
## For Debianish Linux

Declarative package management means you make a file containing everything you want installed, and the package manager adds/removes to make the system match that.  It's good if you happen to install lots of random programs and forget to remove them until you forgot whether they were important to have installed or not in the first place.

Requires: aptitude, sudo, python 3.6+

1. `pip install decapt`
2. `decapt generate`
3. Modify `decapt.luxem`
4. Update your system with `decapt`

## Features

* Manage snap packages
* Manage manual debian packages
  Add an entry like `{ name: mypackage, path: ~/debs/mypackage }` where there's a `build.sh` script in `path` which creates a `mypackage.deb` in `path`.
* [luxem](https://gitlab.com/rendaw/luxem)

## Example

Here's a config similar to what I currently use:
```
{
	installed: [
		* Base packages *
		console-setup,
		linux-image-amd64,
		task-english,
		installation-report,
		task-laptop,
		laptop-detect,
		cryptsetup,
		lvm2,
		ifupdown,
		init,
		grub-efi-amd64, grub-efi-amd64-signed, efibootmgr, shim-signed,
		whiptail,
		gdbm-l10n,
		installation-report,
		util-linux-locales,
		iamerican,
		lsb-release,
		fakeroot,
		geoip-database,
		xdg-user-dirs,
		libnet-dbus-perl, libtie-ixhash-perl,
		libfile-mimeinfo-perl,
		libio-stringy-perl,
		iso-codes,
		opensc,
		publicsuffix,
		libarchive-cpio-perl,
		zenity,
		libltdl-dev,

		* My base *
		fuse,
		aptitude,
		libparse-debianchangelog-perl,
		sudo,
		python3-pip, python3-dev, python3-wheel, python3-keyring, python3-keyrings.alt, python3-xdg, python3-setuptools, python3-venv, python3-tk, python3-virtualenv,
		python3-pyqt5, 
		python-virtualenv, python-dev, python-pip, python-setuptools, virtualenv,
		vim,
		snapd,
		pulseaudio,
		rtkit,

		* Package building + dev *
		cmake,
		meson,
		ninja-build,
		dh-autoreconf,
		libx11-dev,
		libxt-dev,
		libx11-xcb-dev,
		libxkbcommon-x11-dev,
		libxcb-damage0-dev,
		libxcb-sync-dev,
		libxcb-present-dev,
		libxcb-xinput-dev,
		libxcb-cursor-dev,
		libxcb-keysyms1-dev,
		libxcb-randr0-dev,
		libxcb-icccm4-dev,
		libxcb-xinerama0-dev,
		libxcb-xkb-dev,
		libxcb-util0-dev,
		libxcb-ewmh-dev,
		libxcb-composite0-dev,
		libxcb-xrm-dev,
		libxi-dev,
		libgl1-mesa-dev,
		libapt-pkg5.0,
		libapt-inst2.0,
		libdbus-1-dev,
		libconfig-dev,
		libcairo2-dev,
		libpango1.0-dev,
		libjsoncpp-dev,
		libstartup-notification0-dev,
		libev-dev,
		libyajl-dev,
		uthash-dev,
		xcb-proto,
		python-xcbgen,
		libpam0g-dev,
		aspell-en,

		* System config/tuning *
		acpid,
		acpi-support-base,
		linux-cpupower,
		lm-sensors,
		firmware-linux-nonfree, amd64-microcode, intel-microcode,
		tlp,
		ethtool,
		bumblebee-nvidia,
		nvidia-settings,
		libnvidia-cfg1,
		nvidia-persistenced,
		primus,
		libgl1-nvidia-glx,
		nvidia-driver-libs-nonglvnd, libgles-nvidia1, libgles-nvidia2, libglx-nvidia0, nvidia-nonglvnd-vulkan-icd, libnvidia-cfg1, libnvidia-cbl, libnvidia-rtcore, vulkan-utils,
		powertop,
		powermgmt-base,
		tlp-rdw, 
		xsensors, 
		psensor, 
		fancontrol, 
		i8kutils, 
		thermald, 

		* Wireless (someday) *
		iw,
		crda,
		wireless-tools,
		wpasupplicant,

		* Desktop *
		xorg,
		xserver-xorg-legacy,
		libgtk-3-bin,
		gtk2-engines-pixbuf,
		libgdk-pixbuf2.0-bin,
		libcanberra-gtk3-module,
		desktop-base,
		plymouth-label,
		barrier,
		lightdm,
		feh, libjpeg-turbo-progs,
		dunst,
		rofi,
		{ name: i3-gaps, path: ~/soft/i3-gaps },
		{ name: polybar, path: ~/soft/polybar },
		{ name: picom, path: ~/soft/picom },
		{ name: alacritty, path: ~/soft/alacritty },
		{ name: xbanish, path: ~/soft/xbanish },
		gucharmap, * looking up fonts *
		fonts-dejavu,
		fonts-dejavu-extra,
		{ name: paper-icon-theme, path: ~/soft/paper },
		gstreamer1.0-gl,
		gstreamer1.0-libav,
		gstreamer1.0-plugins-good,
		gstreamer1.0-pulseaudio,
		gstreamer1.0-x,
		qt5-gtk-platformtheme,
		libnotify-bin,
		xournal,
		fcitx-frontend-all,
		fcitx-mozc,
		fcitx-config-gtk,
		thunar,
		gimp,

		* CLI *
		pandoc, 
		p7zip-full, p7zip-rar,
		strace,
		curl,
		xclip,
		qrencode,
		dmidecode,
		jq,
		gron,
		dnsutils,
		xdotool,
		xbindkeys,
		htop,
		flameshot,
		dos2unix,
		nethogs,

		* Apps *
		pavucontrol,

		* Firefox *
		firefox-esr,
		libavcodec58,
		va-driver-all,
		vdpau-driver-all,

		* Fonts *
		ttf-unifont,

		* Crypto/sec *
		gpg,
		pinentry-qt,
		{ name: xgopass, path: ~/soft/xgopass },
		pass-extension-otp,
		webext-browserpass,
		yubikey-personalization,
		yubikey-manager,
		libu2f-host0,
		pcscd,
		scdaemon,
		{ name: pamgpg, path: ~/soft/pamgpg },

		* Work *
		git,
		gitg,
		{ name: golang, options: [ -t, buster-backports ] },
		(snap) { name: code, options: [ --classic ] },
		diffuse,
		kdiff3,
		awscli,
		wireshark,
		libpangox-1.0-0, * for cisco anyconnect ui *
		azure-cli,
		google-cloud-sdk,
		libreoffice,
		sqlite3,
		nmap,
		gitlab-runner,
		gdb,
		mpv,
		okular,
		gitk,

		* Python deps *
		libssl-dev,
	],
},
```

A dry run looks like this:
```
$ decapt sync -n ~/soft/decapt.luxem
Scanning current package state for apt...
Scanning current package state for snap...

Plan: Adding--
{
	apt: [
		dnsutils,
		libgssapi-krb5-2,
		libkadm5srv-mit11,
		libmagic-mgc,
		libk5crypto3,
		azure-cli,
		libkrad0,
		libkadm5clnt-mit11,
		libmagic1,
		libsasl2-2,
		libgssrpc4,
		libsasl2-modules-db,
		libkrb5-3,
		libkrb5support0,
	],
	snap: [
	],
},


Plan: Removing--
{
	apt: [
		kcachegrind,
		python3,
		clang,
		globalprotect,
		cdparanoia,
		valgrind,
		libssl1.1,
		git-buildpackage,
		zbar-tools,
		xinput,
		libqt5webkit5,
		openjdk-11-jdk,
		devscripts,
		weechat,
		maven,
		libc6-dbg,
		inkscape,
		libp11-kit0,
		weechat-plugins,
	],
	snap: [
		intellij-idea-community,
	],
},

```

## Notes

### Config format

The config file is a [luxem](https://gitlab.com/rendaw/luxem) file which defaults to `decapt.luxem` in the current directory.

The file has the format
```
{
    installed: [
        PACKAGES...
    ],
}
```
where each package can be

* An apt package name
   ```
   xinput,
   ```

* A custom package builder
   ```
   { name: pamgpg, path: ~/soft/pamgpg },
   ```
   This will run `~/soft/pamgpg/build.sh` and then try to install a `.deb` with the above `name` (ex: `~/soft/pamgpg/pamgpg.deb`).

* A snap
   ```
   (snap) { name: code, options: [ --classic ] },
   ```

### Autoremove

Debian considers _installed_ "suggested" and "recommended" for other installed packages to be important, so they won't be autoremoved after being installed once.  If for instance something recommends chrome or firefox, and you remove chrome from your list and add firefox, it won't be removed (by default).  To make these not recommended, create `/etc/apt/apt.conf.d/99_norecommends` and add:
```
APT::Install-Recommends "false";
APT::AutoRemove::RecommendsImportant "false";
APT::AutoRemove::SuggestsImportant "false";
```
[(source)](https://askubuntu.com/questions/351085/how-to-remove-recommended-and-suggested-dependencies-of-uninstalled-packages)

If you run `apt autoremove` you can confirm what this affects that's currently installed.  You should probably add some to your decapt list (ex: `grub-efi-amd64`).  You can check why something on that list is currently installed by running `aptitude why PACKAGE` -- if it's actually a dependency (not just suggested/recommended) by something else, try `aptitude why` on that something else until you find what package at the head of the chain is set free by this change.
 
### Base packages

`decapt generate` consults the list of currently manually installed non-base software.  Debian seems to "manually" install a lot of unnecessary packages, so the initially generated list will include both packages and dependencies, where you might only want the package itself?  To convert all manually installed dependencies to auto packages, run:

```
$ sudo apt-mark showmanual | while read line; do if [[ $(apt-cache rdepends --installed $line | wc -l) -ne 2 ]]; then apt-mark auto $line; fi; done
```

If you do `decapt generate` after that you should have a smaller amount of base-system spam in the list.

(TODO: `rdepends` seems to include suggested/recommended dependencies too, so this might need to be reworked to use a different tool).

## Friends (not really)

* [Nix](https://nixos.org/nix/) - A strict declarative package (and config) manager, and also the basis of Linux distro [NixOS](https://nixos.org/)
* [Guix](https://guix.gnu.org/) - A strict declarative package (and config) manager, and also the basis of Linux distro GuixSD
* [aconfmgr](https://github.com/CyberShadow/aconfmgr) - A declarative Arch-native package and config manager
* [decpac](https://gitlab.com/rendaw/decpac) - I made something similar for Arch
